
Prequisities
https://www.aliexpress.com/item/2-PCS-5x8mm-Motor-Jaw-Shaft-Coupler-Aluminum-5mm-8mm-Flexible-Coupling-for-3D-Printer-Z/32766147326.html?spm=a2g0s.9042311.0.0.27424c4d0FiEv8
https://www.aliexpress.com/item/100-600mm-3D-Printer-T8-8mm-Rod-Lead-Screw-Nut-Z-Axis-Linear-Rail-Bar-Shaft/32839732117.html?spm=a2g0s.9042311.0.0.27424c4dHN432D


Setup
1. Replace M8 screw to T8 lead screw
2. open https://www.prusaprinters.org/calculator/#steppers and fill correct values (z_default_step.jpg)
3. update DEFAULT_AXIS_STEPS_PER_UNIT
4. upload new firmware
5. check Z axis. Move some cm/mm and check if correct.
