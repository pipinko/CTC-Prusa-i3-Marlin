# Marlin 3D Printer Firmware updated for CTC DIY

<img align="right" src="../../images/CTC_printer.jpg" />
Updated version for CTC DIY. 



**Upload firmware**
- download arduino IDE (https://www.arduino.cc/en/Main/Software)
- edit file boards.txt find atmega2560 upload speed. Rewrite mega.menu.cpu.atmega2560.upload.speed=115200 to mega.menu.cpu.atmega2560.upload.speed=57600
- set:
	Tools -> Board: "Arduino/Genuino Mega or Mega 2560"
	Tools -> Processor: "ATmega2560 (Mega 2560)"
	Tools -> Port (your port)
	Tools -> Programmer: "Arduino as ISP"
- upload firmware

**Original Marlin Resources**

For original Marlin click:
- [Marlin Home Page](http://marlinfw.org/) - The latest Marlin documentation.
- [Marlin Releases](https://github.com/MarlinFirmware/Marlin/releases) - All Marlin releases with release notes.
- [RepRap.org Wiki Page](http://reprap.org/wiki/Marlin) - An overview of Marlin and its role in RepRap.
- [Marlin Firmware Forum](http://forums.reprap.org/list.php?415) - Get help with configuration and troubleshooting.
- [Marlin Firmware Facebook group](https://www.facebook.com/groups/1049718498464482) - Help from the community. (Maintained by [@thinkyhead](https://github.com/thinkyhead).)
- [@MarlinFirmware](https://twitter.com/MarlinFirmware) on Twitter - Follow for news, release alerts, and tips. (Maintained by [@thinkyhead](https://github.com/thinkyhead).)
